﻿using System;
using System.Windows.Forms;

namespace lekcja_5._operatory_mat_i_logiczne_cw1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSprawdz_Click(object sender, EventArgs e)
        {
            int wiek = int.Parse(txtWiek.Text);
            float wzrost = float.Parse(txtWzrost.Text);
            const int minWiek = 14;
            const float minWzrost = 140f;

            /* Operator "||" (lub) sprawdza czy jedno z wyrażeń jest prawdziwe
             * Jeśli tak, to zwraca wynik true
             * Jeśli żadne z wyrażeń nie jest prawdziwe, operator "||" zwróci false
             */
            bool wynik = (wiek >= minWiek || wzrost >= minWzrost);

            lblWynik.Text = wynik.ToString();
        }
    }
}
